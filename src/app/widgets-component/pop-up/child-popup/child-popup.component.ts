import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-child-popup',
  templateUrl: './child-popup.component.html',
  styleUrls: ['./child-popup.component.scss']
})
export class ChildPopupComponent implements OnInit {
  @Input() fisrtNumberValue;
  @Input() secondNumberValue;
  @Output() getResponse = new EventEmitter;
  result: any;

  constructor() { }

  ngOnInit() {
    this.additionBlock();
  }

  additionBlock() {
    this.result = this.fisrtNumberValue + this.secondNumberValue;
    this.getResponse.emit(this.result);
    //this.getResponse.emit('Message from child');
  }
}
