import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildPopupComponent } from './child-popup.component';

describe('ChildPopupComponent', () => {
  let component: ChildPopupComponent;
  let fixture: ComponentFixture<ChildPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
