import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-parent-popup',
  templateUrl: './parent-popup.component.html',
  styleUrls: ['./parent-popup.component.scss']
})

export class ParentPopupComponent implements OnInit {
  additionForm: FormGroup;
  firstNumberValue: number;
  secondNumberValue: number;
  result: number;

  constructor(
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this.additionForm = this.formBuilder.group({
    firstNumber: ['', [Validators.required, Validators.pattern('[0-9]')]],
      secondNumber: ['', [Validators.required, Validators.pattern('[0-9]')]],
      finalOutput: ['',],
    });

    var body = document.body;
    body.classList.add("management-modal");
  }

  getResponse($event) {
    this.result = $event;
  }
  submit() {
    this.firstNumberValue = this.additionForm.get('firstNumber').value;
    this.secondNumberValue = this.additionForm.get('secondNumber').value;
  }
  ngOnDestroy() {
    var body = document.body;
    body.classList.remove("management-modal");

  }
}
