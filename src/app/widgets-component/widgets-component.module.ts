import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ParentPopupComponent } from './pop-up/parent-popup/parent-popup.component';
import { ChildPopupComponent } from './pop-up/child-popup/child-popup.component';

@NgModule({
	imports: [
		CommonModule,
		NgbModule,
		FormsModule,
		ReactiveFormsModule,

	],
	declarations: [
		ParentPopupComponent,
		ChildPopupComponent,
	],
	entryComponents: [
		ParentPopupComponent,
	],
	exports: []
})

export class WidgetsComponentModule { }
