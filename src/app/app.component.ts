import { Component } from '@angular/core';
import { NgxUiLoaderService } from 'ngx-ui-loader'
import { CoreService } from './services/core.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})

export class AppComponent {
  title = 'InterviewTask';

  constructor(
    private ngxService: NgxUiLoaderService,
    private coreService: CoreService,
  ) { }

  showPopup() {
    this.coreService.showTimeLogDialog();
  }
}
