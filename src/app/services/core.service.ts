import { Injectable } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ParentPopupComponent } from '../widgets-component/pop-up/parent-popup/parent-popup.component';

@Injectable({
  providedIn: 'root'
})

export class CoreService {

  constructor(private modalService: NgbModal) { }

  /**
  * showTimeLogDialog function is used to open showTimeLog Dialog Component.
  */
  showTimeLogDialog() {
    const modalRef = this.modalService.open(ParentPopupComponent);
    return modalRef.result;
  }

}
